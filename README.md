# jar 파일 생성
```shell
$ gradle wrap

$ gradlew build
```

# 이미지를 저장소에 올리기
buildah에서 docker hub에 로그인 후 Dockerfile이 위치한 경로에서 실행한다.
```shell
$ buildah login docker.io

$ buildah bud -t docker.io/wonyj210/demo .

$ buildah push docker.io/wonyj210/demo:latest
```

# namespace와 istio injection 설정
```shell
$ kubectl create namespace realten

$ kubectl label namespace realten istio-injection=enabled
```

# 샘플 1
URI 경로에 따라 라우팅하는 방법에 대한 샘플
```shell
$ kubectl apply -f configmap.yaml

$ kubectl apply -f istio-setting.yaml

$ kubectl apply -f demo-v1.yaml

$ kubectl apply -f demo-v2.yaml
```

# 샘플 2
subset을 사용하는 방법과 가중치 설정 방법에 대한 샘플
```shell
$ kubectl apply -f configmap.yaml

$ kubectl apply -f istio-subset.yaml
```

# 테스트
1. /etc/hosts 파일에 아래와 같이 도메인 주소 설정
    ```shell
    NAMESPACE        NAME                           TYPE           CLUSTER-IP       EXTERNAL-IP    PORT(S)                                                                      AGE     SELECTOR
    istio-system     service/istio-ingressgateway   LoadBalancer   10.99.86.50      10.100.0.111   15021:31037/TCP,80:30514/TCP,443:32042/TCP,31400:32176/TCP,15443:32651/TCP   3d19h   app=istio-ingressgateway,istio=ingressgateway
    ```
    해당 IP는 istio-ingressgateway 외부 주소
    ```
    10.100.0.111 demo.realten.com
    ```
2. API 호출
   * 샘플 1의 경우
   ```shell
   $ curl http://demo.realten.com/v1/health
   ```
   * 샘플 2의 경우
   ```shell
    $ curl http://demo.realten.com/health
    ```
   
# Istio기반 Jaeger Trace
Istio에서 Jaeger를 통해 로그 추적을 할 수 있다.
Envoy Proxy에서 Trace에 관련된 헤더 값을 자동으로 추가하는데
어플리케이션에서 다른 서비스로 통신할 경우 하나의 Trace로 묶이기 위해선
다른 서비스로 요청할 때의 Request Header에 Trace 관련 헤더 값을 전달해야 한다.
전달에 필요한 헤더 목록은 아래와 같다.
- x-request-id
- x-b3-traceid
- x-b3-spanid
- x-b3-parentspanid
- x-b3-sampled
- x-b3-flags

서비스에서 소스 구현 시 아래와 같이 헤더 값을 추가하도록 하자.
```java
.headers(httpHeaders -> {
     while(headerNames.hasMoreElements()) {
        String name = headerNames.nextElement();
        if("x-request-id".equals(name) || name.startsWith("x-b3")) {
            httpHeaders.add(name, request.getHeader(name));
        }
        log.info("header - name : {}, value : {}", name, request.getHeader(name));
    }
})
```
