package com.realten.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.Enumeration;

@Slf4j
@RestController
public class DemoController {
    @Value("${spring.application.name}")
    private String appName;

    @GetMapping("/health")
    public String health() {
        log.info("{} health check call", appName);
        return appName + " OK";
    }

    /**
     * K8S에서 readinessProbe 설정 시 연결할 API
     * @return
     */
    @GetMapping("/health/readiness")
    public String readinessHealth() {
        log.info("{} readiness health check call", appName);
        return appName + " OK";
    }

    /**
     * K8S에서 livenessProbe 설정 시 연결할 API
     * @return
     */
    @GetMapping("/health/liveness")
    public String livenessHealth() {
        log.info("{} liveness health check call", appName);
        return appName + " OK";
    }

    @GetMapping("/health/startup")
    public String startupHealth() {
        log.info("{} startup health check call", appName);
        return appName + " OK";
    }

    @GetMapping("/session")
    public String getSessionId(HttpSession httpSession) {
        return httpSession.getId() + ", " + LocalDateTime.now();
    }

    /**
     * 서비스 간 통신
     *
     * @param request
     * @return
     */
    @GetMapping("/ping")
    public String ping(HttpServletRequest request) {
        Enumeration<String> headerNames = request.getHeaderNames();
        WebClient webClient = WebClient.builder().baseUrl("http://demo-service-v2:8080").build();
        String s = webClient.get()
                .uri(uriBuilder -> uriBuilder.path("/pong").build())
                .headers(httpHeaders -> {
                    while(headerNames.hasMoreElements()) {
                        String name = headerNames.nextElement();
                        if("x-request-id".equals(name) || name.startsWith("x-b3")) {
                            httpHeaders.add(name, request.getHeader(name));
                        }
                        log.info("header - name : {}, value : {}", name, request.getHeader(name));
                    }
                })
                .retrieve()
                .bodyToMono(String.class)
                .log()
                .block();
        return s;
    }

    @GetMapping("/pong")
    public String pong() {
        return "pong";
    }
}
